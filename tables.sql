CREATE TABLE tb_user(
	id SERIAL,
	
  str_first_name VARCHAR(50),
  str_last_name VARCHAR(50),
	
	dt_created_at TIMESTAMP WITH TIME ZONE,
  dt_updated_at TIMESTAMP WITH TIME ZONE
);
ALTER TABLE tb_user ADD CONSTRAINT pk_user PRIMARY KEY (id);


CREATE TABLE tb_email(
	id SERIAL,
	
  id_user_from INTEGER,
  id_user_to INTEGER,
  str_message VARCHAR(200),
	
	dt_created_at TIMESTAMP WITH TIME ZONE,
  dt_updated_at TIMESTAMP WITH TIME ZONE
);
ALTER TABLE tb_email ADD CONSTRAINT pk_email PRIMARY KEY (id);
ALTER TABLE tb_email ADD CONSTRAINT fk_userto_email 
  FOREIGN KEY (id_user_to) REFERENCES tb_user(id);
ALTER TABLE tb_email ADD CONSTRAINT fk_userfrom_email 
  FOREIGN KEY (id_user_from) REFERENCES tb_user(id);