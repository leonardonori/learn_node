'use strict';
module.exports = (sequelize, DataType) =>{
  const Email = sequelize.define("Email",{
    id:{ type: DataType.INTEGER,primaryKey: true,autoIncrement: true,field: "id" },
    fromUserId:{ type: DataType.INTEGER, field: 'id_user_from' },
    toUserId:{ type: DataType.INTEGER, field: 'id_user_to' },
    message:{ type: DataType.STRING, field: 'str_message' },
    createdAt: { type: DataType.DATE, field: 'dt_created_at' },
    updatedAt: { type: DataType.DATE, field: 'dt_updated_at' }
  },{
    classMethods:{
      associate: function(models) {
        Email.belongsTo(models.User, {
          as: 'emailsSent',
          foreignKey: 'fromUserId'
        });
        Email.belongsTo(models.User, {
          as: 'emailsReceived',
          foreignKey: 'toUserId'
        });
      }
    },
    tableName: 'tb_email',
    freezeTableName: true,
    timestamps: true,
    onDelete: 'CASCADE'
  });
  return Email;
};