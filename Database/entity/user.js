'use strict';
module.exports = (sequelize, DataType) =>{
  const User = sequelize.define("User",{
    id: { type: DataType.INTEGER, primaryKey: true, autoIncrement: true, field: "id"},
    firstName: { type: DataType.STRING, field: 'str_first_name' },
    lastName: { type: DataType.STRING,field: 'str_last_name'},
    createdAt: { type: DataType.DATE, field: 'dt_created_at'},
    updatedAt: { type: DataType.DATE, field: 'dt_updated_at'}
  },{
    classMethods:{
      associate: function(models) {
        User.hasMany(models.Email, {
          as: 'emailsSent',
          foreignKey: 'fromUserId'
        });

        User.hasMany(models.Email, {
          as: 'emailsReceived',
          foreignKey: 'toUserId'
        });

      }
    },
    tableName: 'tb_user',
    freezeTableName: true,
    timestamps: true,
    onDelete: 'CASCADE'
  });
  return User;
};