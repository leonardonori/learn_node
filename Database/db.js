'use strict';

import fs from "fs";
import path from "path";
import Sequelize from "sequelize";

const config = require('../config/dbconfig.js');

let sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config.params
);

let db = {
  sequelize,
  Sequelize,
  models: {}
};

const dir = path.join(__dirname, "entity");
fs.readdirSync(dir).forEach( file => {
  const modelDir = path.join(dir, file);
  const model = sequelize.import(modelDir);
  console.log(model);
  db.models[model.name] = model;
});

Object.keys(db.models).forEach( key => {
  db.models[key].associate(db.models);
});

module.exports = db;