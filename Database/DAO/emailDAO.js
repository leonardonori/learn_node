'use strict';

import db from "../db.js";

const User = db.models.User;
const Email = db.models.Email;

let EmailDAO = { };

EmailDAO.create = function (object, callback){
  Email.create(object)
    .then(result => { callback(null, result); })
    .catch(err => { callback(err, null); });
};

EmailDAO.selectById = function(id, callback){
  Email.findOne( { where: {id: id} } )
  .then(result => { callback(null, result); })
  .catch(err => { callback(err, null); });
}

EmailDAO.loadEmailById = function(id, callback){
  Email.findOne({ 
    where: {
      id: id
    },
    include: [
      {model: User, as: 'userFrom'},
      {model: User, as: 'fromUserId'}
    ]
  })
  .then(result => { callback(null, result); })
  .catch(err => { callback(err, null); });
}

EmailDAO.update = function(object, callback){
  Email.update( object, { where: {id: id}, returning: true } )
  .then(result => { callback(null, result[1]); })
  .catch(err => { callback(err, null); });
}

module.exports = EmailDAO;