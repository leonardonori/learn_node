'use strict';

import db from "../db.js";

const User = db.models.User;
const Email = db.models.Email;

let UserDAO = { };

UserDAO.create = function (object, callback){
  User.create(object)
    .then(result => { callback(null, result); })
    .catch(err => { callback(err, null); });
};

UserDAO.selectById = function(id, callback){
  User.findOne( { where: {id: id} } )
  .then(result => { callback(null, result); })
  .catch(err => { callback(err, null); });
}

UserDAO.loadUserById = function(id, callback){
  User.findOne({ 
    where: {
      id: id
    },
    include: [
      {model: Email, as: 'emailsSent'},
      {model: Email, as: 'emailsReceived'}
    ]
  })
  .then(result => { callback(null, result); })
  .catch(err => { callback(err, null); });
}

UserDAO.update = function(object, callback){
  User.update( object, { where: {id: id}, returning: true } )
  .then(result => { callback(null, result[1]); })
  .catch(err => { callback(err, null); });
}

module.exports = UserDAO;