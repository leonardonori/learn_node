'use strict'

import emailDAO from "../Database/DAO/emailDAO"
import userBO from "./userBO"

let EmailBO = { };

EmailBO.create = function(user, callback){
  emailDAO.create(user, callback);
};

EmailBO.selectById = function (id, callback){
  emailDAO.selectById(id, callback);
};

EmailBO.loadUserById = function(id, callback){
  emailDAO.selectById(id, callback);
};

EmailBO.update = function(user, callback){
  emailDAO.update(id, callback);
};

module.exports = EmailBO;