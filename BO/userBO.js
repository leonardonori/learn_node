'use strict'

import userDAO from "../Database/DAO/userDAO"
import emailBO from "./emailBO"

let userBO = { };

userBO.create = function(user, callback){
  userDAO.create(user, callback);
};

userBO.selectById = function (id, callback){
  userDAO.selectById(id, callback);
};

userBO.loadUserById = function(id, callback){
  userDAO.selectById(id, callback);
};

userBO.update = function(user, callback){
  userDAO.update(id, callback);
};

module.exports = userBO;