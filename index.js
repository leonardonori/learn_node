import consign from "consign";
import express from "express";
import bodyParser from "body-parser";

const app = express();

app.set("port", 5000);
app.set("json space", 4);
app.use(bodyParser.json());

consign()
  .include("./Database/db.js")
  .then("./routes")
  .into(app);

module.exports = app;

app.listen(app.get("port"), ()=>{
   console.log(`Delivery Center on - porta ${app.get("port")}`);
  } 
);

app.Database.db.sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });