import EmailBO from "../BO/emailBO"

module.exports = app => {
  app.route("/mail/:id").get( (req, res) => { 
    EmailBO.selectById(req.params.id, function(err, User){
      if(err){
        res.status(200).json(err);
      }else{
        res.status(200).json(User);
      }
    });
  });
  app.route("/mail").post( (req, res) => { 
    EmailBO.create(req.body, function(err, User){
      if(err){
        res.status(200).json(err);
      }else{
        res.status(200).json(User);
      }
    });
  });
}