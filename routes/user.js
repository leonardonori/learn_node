import UserBO from "../BO/userBO"

module.exports = app => {
  app.route("/user/:id").get( (req, res) => { 
    UserBO.selectById(req.params.id, function(err, User){
      if(err){
        res.status(200).json(err);
      }else{
        res.status(200).json(User);
      }
    });
  });

  app.route("/user/:id/mails").get( (req, res) => { 
    UserBO.loadUserById(req.params.id, function(err, User){
      if(err){
        res.status(200).json(err);
      }else{
        res.status(200).json(User);
      }
    });
  });

  app.route("/user").post( (req, res) => { 
    UserBO.create(req.body, function(err, User){
      if(err){
        res.status(200).json(err);
      }else{
        res.status(200).json(User);
      }
    });
  });
}