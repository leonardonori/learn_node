module.exports = {
  database : "node1",
  username : "postgres",
  password : "root",
  params:{
    host: "127.0.0.1",
    dialect : "postgres",
    logging: (sql) => {
      console.log(`${sql}`);
    }
  }
};